muensingen <- read.csv2("muensingen_fib.csv")
head(muensingen)
my_table <- table(muensingen$fibula_scheme, muensingen$Grave) 
my_table
addmargins(my_table)
plot(muensingen$Length)
plot(muensingen$Length,type="b")
plot(muensingen$fibula_scheme)
plot(muensingen$Length, muensingen$FL,
     xlim=c(0, 140), # limits of the x axis
     ylim = c(0, 100), # limits of the y axis
     xlab = "Fibula Length", # label of the y axis
     ylab = "Foot Length", # label of the x axis
     main = "Fibula total length vs. Foot Length", # main title
     sub="example plot" # subtitle
)
abline(v = mean(muensingen$Length), col = "red")
abline(h = mean(muensingen$FL), col = "green")
abline(lm(FL~Length, data = muensingen), col = "blue")
table(muensingen$fibula_scheme)
pie(table(muensingen$fibula_scheme))
pie(table(muensingen$fibula_scheme),
    col=c("red","green","blue"))
barplot(table(muensingen$fibula_scheme))
barplot(muensingen$Length)
par(las=2) 
barplot(muensingen$Length,
        names.arg=muensingen$Grave)
title("Fibulae length")
par(las=1)
barplot(table(muensingen$fibula_scheme), 
        horiz=T,                          
        cex.names=2)      
my_new_table <- table(muensingen$fibula_scheme,
                      muensingen$Coils)
my_new_table
barplot(my_new_table)
barplot(my_new_table, beside=T, legend.text=T)
table.prop<-prop.table(my_new_table,2)
table.prop
barplot(table.prop)
tmp<-barplot(table.prop,
             legend.text=T,  # add a legend
             col=rainbow(3)  # make it more colorful
)
title("ratio of fibulae schemes \n by number of coils",
      outer=TRUE,            # outside the plot area
      line=- 3)
par(mfrow=c(2,1))
barplot(my_new_table,beside=T)
barplot(table.prop,beside=T)
par(mfrow=c(1,2))
barplot(muensingen$Length[1:2],xpd=F,ylim=c(45,55))
barplot(muensingen$Length[1:2],xpd=F)


boxplot(1:9)
boxplot(muensingen$Length)
boxplot(muensingen$Length ~
          muensingen$fibula_scheme)
par(las=1)
boxplot(Length ~ fibula_scheme,
        data = muensingen,
        main = "Length by type",
        col="grey",
        xlab="fibulae scheme",
        ylab= "length"
)
plot(muensingen$Length, muensingen$FL)
abline(
  lm(muensingen$FL~muensingen$Length),
  col="red")

install.packages("car")

library(car) # library for regression analysis
scatterplot(FL ~ Length, data = muensingen)